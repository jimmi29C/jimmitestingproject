//
//  DetailTableCell.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class DetailTableCell: UITableViewCell {
    
    let contentStack = UIStackView()
    let idLbl = UILabel()
    let nameLbl = UILabel()
    let salaryLbl = UILabel()
    let ageLbl = UILabel()
    
    let buttonView = UIView()
    let editBtn = UIButton()
    let deleteBtn = UIButton()
    
    var disposeBag = DisposeBag()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.isHidden = true
        selectionStyle = .none
        
        setupSubviews([contentStack, buttonView])
        contentStack.setupArrangedSubviews([idLbl, nameLbl, salaryLbl, ageLbl])
        buttonView.setupSubviews([editBtn, deleteBtn])
        
        setupConstraints()
        setupCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(model: DetailModel) {
        idLbl.text = "ID: " + model.id
        nameLbl.text = "Name: " + model.employee_name
        salaryLbl.text = "Salary: " + model.employee_salary
        ageLbl.text = "Age: " + model.employee_age
    }
    
    func setupCell() {
        
        contentStack.axis = .vertical
        contentStack.spacing = 4
        
        editBtn.setTitle("Edit", for: .normal)
        
        deleteBtn.setTitle("Delete", for: .normal)
        
        [editBtn, deleteBtn].setup {
            $0.backgroundColor = .black
            $0.setLayer(cornerRadius: 10)
            $0.setTitleColor(.white, for: .normal)
        }
        
    }
    
    func setupConstraints() {
        
        contentStack.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(self)
        }
        
        buttonView.snp.makeConstraints { (make) in
            make.top.equalTo(contentStack.snp.bottom).offset(20)
            make.centerX.equalTo(self)
            make.bottom.equalTo(self).offset(-20)
        }
        
        editBtn.snp.makeConstraints { (make) in
            make.top.leading.bottom.equalTo(buttonView)
            make.width.equalTo(100)
        }
        
        deleteBtn.snp.makeConstraints { (make) in
            make.top.trailing.bottom.equalTo(buttonView)
            make.leading.equalTo(editBtn.snp.trailing).offset(10)
            make.width.equalTo(100)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}
