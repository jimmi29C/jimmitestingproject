//
//  DetailRouter.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit

final class DetailRouter: DetailRouterDelegate {
    
    weak var source: UIViewController?
    
    init(view: DetailViewControllerDelegate?) {
        source = view as? UIViewController
    }
    
    func pushToUpdate(status: AllEnum, id: String) {
        let vc = HomeViewController()
        vc.initialize(status: status, id: id)
        source?.navigationController?.pushViewController(vc, animated: true)
    }
}




