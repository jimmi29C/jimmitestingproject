//
//  DetailViewController.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit
import RxSwift
import RxCocoa
import Alamofire
import SwiftyJSON

class DetailViewController: UIViewController {
    
    let root = DetailView()
    lazy var presenter: DetailPresenterDelegate = DetailPresenter(view: self)
    
    var disposeBag = DisposeBag()
    
    var detailData: [DetailModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = root
        
        presenter.read()
        
        root.detailTable.delegate = self
        root.detailTable.dataSource = self
        
        setRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    private func setRX() {
        
    }
}

extension DetailViewController: DetailViewControllerDelegate {
    
    func successRead(data: [DetailModel]) {
        self.detailData = data
        root.detailTable.reloadData()
    }
    
    func failureRead(_ error: Error) {
        
    }
    
}

extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DetailTableCell
        let model = detailData[indexPath.row]
        cell.initialize(model: detailData[indexPath.row])
        
        
        cell.editBtn.rx.tap.subscribe(onNext: { (_) in
            self.presenter.pushToUpdate(status: .update, id: model.id)
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        
        cell.deleteBtn.rx.tap.subscribe(onNext: { (_) in
            self.presenter.pushToUpdate(status: .delete, id: model.id)
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        return cell
    }
    
    
}
