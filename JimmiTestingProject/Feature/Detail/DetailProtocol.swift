//
//  DetailProtocol.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit

protocol DetailViewControllerDelegate: AnyObject {
    func successRead(data: [DetailModel])
    func failureRead(_ error: Error)
    
}

protocol DetailPresenterDelegate: AnyObject {
    func read()
    func pushToUpdate(status: AllEnum, id: String)
    
}

protocol DetailRouterDelegate: AnyObject {
    func pushToUpdate(status: AllEnum, id: String)
}

protocol DetailInteractorInputDelegate: AnyObject {
    func read()
    
}

protocol DetailInteractorOutputDelegate: AnyObject {
    func successRead(data: [DetailModel])
    func failureRead(_ error: Error)
}









