//
//  DetailInteractor.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit
import Alamofire
import SwiftyJSON

final class DetailInteractor {
    weak var presenter: DetailInteractorOutputDelegate?
    
    init(presenter: DetailInteractorOutputDelegate) {
        self.presenter = presenter
    }
    
}

extension DetailInteractor: DetailInteractorInputDelegate {
    
    func read() {
        let url = URL(string: "http://dummy.restapiexample.com/api/v1/employees")!
        
        let headers: [String: String] = [
            "Content-Type": "application/json"
        ]
        
        AF.request(url, method: .get, headers: HTTPHeaders(headers))//    , encoding: URLEncoding.httpBody)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
            switch response.result {
                case .success(let data):
                print("Success with JSON: \(data)")
                let json = JSON(data)
                let detailData = json["data"].arrayValue.map { (value) in
                    DetailModel(json: value)
                }
                self.successRead(data: detailData)
                case .failure(let error):
                    print("Request failed with error: \(error)")
                }
        }
    }
    
    
}

extension DetailInteractor: DetailInteractorOutputDelegate {
    
    func successRead(data: [DetailModel]) {
        presenter?.successRead(data: data)
    }
    
    func failureRead(_ error: Error) {
        presenter?.failureRead(error)
    }
    

}


