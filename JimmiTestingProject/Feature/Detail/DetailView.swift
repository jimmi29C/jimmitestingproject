//
//  DetailView.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import SnapKit

class DetailView: UIView {
    
    let titleLbl = UILabel()
    let detailTable = UITableView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews([titleLbl, detailTable])
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        titleLbl.text = "Employee List"
        titleLbl.textColor = .white
        titleLbl.backgroundColor = .yellow
        
        detailTable.register(DetailTableCell.self, forCellReuseIdentifier: "cell")
        detailTable.allowsSelection = false
    }
    
    func setupConstraints() {
        
        titleLbl.snp.makeConstraints { (make) in
            make.top.equalTo(safeAreaLayoutGuide.snp.topMargin)
            make.leading.trailing.equalTo(self)
        }
        
        detailTable.snp.makeConstraints { (make) in
            make.top.equalTo(titleLbl.snp.bottom)
            make.leading.trailing.equalTo(self)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottomMargin)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}

