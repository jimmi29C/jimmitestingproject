//
//  DetailPresenter.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit

final class DetailPresenter {
    weak var view: DetailViewControllerDelegate?
    lazy var router: DetailRouterDelegate? = DetailRouter(view: view)
    lazy var interactor: DetailInteractorInputDelegate = DetailInteractor(presenter: self)
    
    init(view: DetailViewControllerDelegate?) {
        self.view = view
    }
}

extension DetailPresenter: DetailPresenterDelegate {
    func read() {
        interactor.read()
    }
    
    func pushToUpdate(status: AllEnum, id: String) {
        router?.pushToUpdate(status: status, id: id)
    }
    
}

extension DetailPresenter: DetailInteractorOutputDelegate {
    
    func successRead(data: [DetailModel]) {
        view?.successRead(data: data)
    }
    
    func failureRead(_ error: Error) {
        view?.failureRead(error)
    }
    
}
