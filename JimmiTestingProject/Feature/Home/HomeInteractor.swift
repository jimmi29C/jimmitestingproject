//
//  HomeInteractor.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit
import Alamofire
import SwiftyJSON

final class HomeInteractor {
    weak var presenter: HomeInteractorOutputDelegate?
    
    init(presenter: HomeInteractorOutputDelegate) {
        self.presenter = presenter
    }
    
}

extension HomeInteractor: HomeInteractorInputDelegate {
    
    func create(param: [String: Any]) {
        let url = URL(string: "http://dummy.restapiexample.com/api/v1/create")!
        
        let headers: [String: String] = [
            "Content-Type": "application/json"
        ]
        
        AF.request(url, method: .post, headers: HTTPHeaders(headers))//	, encoding: URLEncoding.httpBody)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
            switch response.result {
                case .success(let data):
                print("Success with JSON: \(data)")
                let json = JSON(data)
                
                case .failure(let error):
                    print("Request failed with error: \(error)")
                }
        }
    }
    
    func api(status: AllEnum, id: String, param: [String: Any]) {
        var url = URL(string: "http://dummy.restapiexample.com/api/v1/\(status)")!
        if id != "" {
            let url = URL(string: "http://dummy.restapiexample.com/api/v1/\(status)/\(id)")!
        }
        
        let headers: [String: String] = [
            "Content-Type": "application/json"
        ]
        
        var method: HTTPMethod = .get
        switch status {
        case .create:
            method = .post
        case .update:
            method = .put
        case .delete:
            method = .delete
        }
        
            AF.request(url, method: method , headers: HTTPHeaders(headers))//    , encoding: URLEncoding.httpBody)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
            switch response.result {
                case .success(let data):
                print("Success with JSON: \(data)")
                let json = JSON(data)
                
                case .failure(let error):
                    print("Request failed with error: \(error)")
                }
        }
    }
    
}

extension HomeInteractor: HomeInteractorOutputDelegate {
    
    func successCreate(id: String) {
        presenter?.successCreate(id: id)
    }
    
    func failureCreate(error: Error) {
        presenter?.failureCreate(error: error)
    }

}


