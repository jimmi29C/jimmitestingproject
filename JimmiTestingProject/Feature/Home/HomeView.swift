//
//  HomeView.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import SnapKit

class HomeView: UIView {
    
    let contentView = UIView()
    let contentStack = UIStackView()
    let nameTF = UITextField()
    let salaryTF = UITextField()
    let ageTF = UITextField()
    let submitBtn = UIButton()
    let showAllBtn = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews([contentView, showAllBtn])
        contentView.setupSubviews([contentStack, submitBtn])
        contentStack.setupArrangedSubviews([nameTF, salaryTF, ageTF])
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        contentView.backgroundColor = .yellow
        
        contentStack.axis = .vertical
        contentStack.spacing = 10
        
        nameTF.placeholder = "Name"
        
        salaryTF.placeholder = "Salary"
        
        ageTF.placeholder = "Age"
        
        [nameTF, salaryTF, ageTF].setup {
            $0.setLayer(borderWidth: 1, borderColor: .black)
            $0.textAlignment = .center
            $0.backgroundColor = .white
        }
        
        submitBtn.backgroundColor = .black
        submitBtn.setLayer(cornerRadius: 10)
        submitBtn.setTitle("Submit", for: .normal)
        
        showAllBtn.setTitle("Show All Employees", for: .normal)
        showAllBtn.backgroundColor = .blue
        
        [submitBtn, showAllBtn].setup {
            $0.setTitleColor(.white, for: .normal)
        }
        
    }
    
    func setupConstraints() {
        
        contentView.snp.makeConstraints { (make) in
            make.top.equalTo(safeAreaLayoutGuide.snp.topMargin)
            make.leading.trailing.equalTo(self)
        }
        
        contentStack.snp.makeConstraints { (make) in
            make.centerY.equalTo(contentView).offset(-30)
            make.leading.equalTo(contentView).offset(50)
            make.trailing.equalTo(contentView).offset(-50)
        }
        
        [nameTF, salaryTF, ageTF].setup {
            $0.snp.makeConstraints { (make) in
                make.height.equalTo(60)
            }
        }
        
        submitBtn.snp.makeConstraints { (make) in
            make.top.equalTo(contentStack.snp.bottom).offset(20)
            make.centerX.equalTo(contentStack)
            make.width.equalTo(contentView).dividedBy(3)
            make.height.equalTo(50)
        }
        
        showAllBtn.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.snp.bottom)
            make.leading.trailing.equalTo(contentView)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottomMargin)
            make.height.equalTo(60)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}

