//
//  HomeProtocol.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit

protocol HomeViewControllerDelegate: AnyObject {
    func successCreate(id: String)
    func failureCreate(error: Error)
}

protocol HomePresenterDelegate: AnyObject {
    func create(param: [String: Any])
    func pushToDetail()
    func api(status: AllEnum, id: String, param: [String: Any])
}

protocol HomeRouterDelegate: AnyObject {
    func pushToDetail()
    
}

protocol HomeInteractorInputDelegate: AnyObject {
    func create(param: [String: Any])
    func api(status: AllEnum, id: String, param: [String: Any])
}

protocol HomeInteractorOutputDelegate: AnyObject {
    func successCreate(id: String)
    func failureCreate(error: Error)
}









