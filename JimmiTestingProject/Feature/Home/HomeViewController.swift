//
//  HomeViewController.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit
import RxSwift
import RxCocoa
import Alamofire
import SwiftyJSON

class HomeViewController: UIViewController {
    
    let root = HomeView()
    lazy var presenter: HomePresenterDelegate = HomePresenter(view: self)
    
    var disposeBag = DisposeBag()
    
    var status: AllEnum = .create
    var id: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = root
        
        setRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func initialize(status: AllEnum, id: String) {
        self.status = status
        self.id = id
        if id == "" {
            root.showAllBtn.isHidden = true
        }
    }
    
    func checkEmptyTF() {
        guard let name = root.nameTF.text else { presentAlert(); return }
        guard let salary = root.salaryTF.text else { presentAlert(); return }
        guard let age = root.ageTF.text else { presentAlert(); return }
        
        let param: [String: Any] = [
            "name": name,
            "salary": salary,
            "age": age
        ]
        
        presenter.api(status: self.status, id: self.id, param: param)
    }
    
    func presentAlert() {
        
    }
    
    private func setRX() {
        root.submitBtn.rx.tap.subscribe(onNext: { (_) in
            self.checkEmptyTF()
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        
        root.showAllBtn.rx.tap.subscribe(onNext: { (_) in
            self.presenter.pushToDetail()
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)

    }
}

extension HomeViewController: HomeViewControllerDelegate{
    
    func successCreate(id: String) {
        
    }
    
    func failureCreate(error: Error) {
        
    }
    
}
