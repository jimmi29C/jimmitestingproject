//
//  HomeRouter.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit

final class HomeRouter: HomeRouterDelegate {
    
    weak var source: UIViewController?
    
    init(view: HomeViewControllerDelegate?) {
        source = view as? UIViewController
    }
    
    func pushToDetail() {
        let vc = DetailViewController()
        source?.navigationController?.pushViewController(vc, animated: true)
    }
    
}




