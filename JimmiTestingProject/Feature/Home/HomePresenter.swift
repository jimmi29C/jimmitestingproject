//
//  HomePresenter.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit

final class HomePresenter {
    weak var view: HomeViewControllerDelegate?
    lazy var router: HomeRouterDelegate? = HomeRouter(view: view)
    lazy var interactor: HomeInteractorInputDelegate = HomeInteractor(presenter: self)
    
    init(view: HomeViewControllerDelegate?) {
        self.view = view
    }
}

extension HomePresenter: HomePresenterDelegate {
    
    func create(param: [String: Any]) {
        interactor.create(param: param)
    }
    
    func pushToDetail() {
        router?.pushToDetail()
    }
    
    func api(status: AllEnum, id: String, param: [String: Any]) {
        interactor.api(status: status, id: id, param: param)
    }
}

extension HomePresenter: HomeInteractorOutputDelegate {
    
    func successCreate(id: String) {
        view?.successCreate(id: id)
    }
    
    func failureCreate(error: Error) {
        view?.failureCreate(error: error)
    }
    
    
}
