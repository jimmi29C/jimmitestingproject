//
//  AllEnum.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit

enum AllEnum {
    case create
    case update
    case delete
}
