//
//  DetailModel.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import SwiftyJSON

class DetailModel {
    
    let employee_age: String
    let employee_name: String
    let employee_salary: String
    let id: String
    let profile_iamge: String
    
    init(json: JSON) {
        
        employee_age = json["employee_age"].stringValue
        employee_name = json["employee_name"].stringValue
        employee_salary = json["employee_salary"].stringValue
        id = json["id"].stringValue
        profile_iamge = json["profile_iamge"].stringValue
    }
}
