//
//  UIStackView.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit

extension UIStackView {
    public func setupArrangedSubviews(_ views: [UIView]) {
        for item in views {
            addArrangedSubview(item)
        }
    }
}
