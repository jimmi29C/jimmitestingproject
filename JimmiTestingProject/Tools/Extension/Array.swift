//
//  Array.swift
//  JimmiTestingProject
//
//  Created by Jimmi Oh on 02/10/20.
//

import UIKit

extension Array where Element: UIView {
    
    func setup(_ callback: ((Element)->Void)) {
        for item in self {
            callback(item)
        }
    }
    
}
